## Important 

To use this program you must already have the ESRI package software already downloaded to your C:\ drive.  Additionally, in order to run this code, you must use the MOJO compile and run tools: 
- **cd C:\esri\MOJ20\examples**
- **moj_compile progname.java**
- **moj_run progname .**

## CoordinateLine Usage

Enter coordinates of latitude and longitude degrees as decimal numbers in the available text fields. Using the the dropdown menu, specify the cardinal direction, North, East, South, orWest, for each degree of latitude or longitude. If the user mistypes or enters an invalid entry into the text fields, they will be notified on screen and no coordinate lines will be drawn. Click on one of the line color buttons or you can use the default black line color. Once the user has entered two coordinate pairs and selected a line color, two points will be drawn on the map and a line will be slowly drawn between them. The user may then clear their entries and draw more lines between points on the map (as many as they want). 

**SPECIAL NOTE:** This map depicts the United States of America and excludes surrounding countries. Additionally, for meaningful use and being able to actually see the lines on the screen, the user should enter points that are North latitude and West longitude. However, the user has the option to draw a line that goes beyond the bounds of the window. 

## CoordinateTriangle Usage

Enter coordinates of latitude and longitude degrees as decimal numbers in the available text fields. Using the the dropdown menu, specify the cardinal direction, North, East, South, or West, for each degree of latitude or longitude. If the user mistypes or enters an invalid entry into the text fields, they will be notified on screen and no coordinate lines will be drawn. Click on one of the line color buttons or you can use the default black line color. Once the user has entered three coordinate pairs and selected a line color, three points will be drawn on the map and lines will be slowly drawn between them to form a triangle. The user may then clear their entries and draw more triangles of points on the map (as many as they want). 

**SPECIAL NOTE:** This map depicts the United States of America and excludes surrounding countries. Additionally, for meaningful use and being able to actually see the lines on the screen, the user should enter points that are North latitude and West longitude. However, the user has option to draw a line to a coordinate that goes beyond the bounds of the window. 
