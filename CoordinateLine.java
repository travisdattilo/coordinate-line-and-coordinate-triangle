// Just adds a simple AcetateLayer to place a line & points on the
// map. Mercifully this works if the drawing is done in Java terms,
// using frame coordinates (pixels).  Using an AcetateLayer has the
// advantage that an AcetateLayer is transparent, and this solves
// a problem that transparent layers in Java are somewhat tricky
// to do, e.g. a canvas can not be made transparent. In addition,
// the line is drawn in stages by using a thread.  The thread is
// wise and necessary.  In this  version, the acetate layers
// are removed, within the loop, so they do not accumulate
// and slow things down. The loop should work with two arbitrary
// points!  Those points are parameters to the thread code.

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import java.awt.event.*;
import java.awt.*;
import java.awt.geom.*;
import com.esri.mo2.ui.bean.*;
import com.esri.mo2.map.draw.*;
import com.esri.mo2.cs.geom.Point;
import com.esri.mo2.ui.tb.ZoomPanToolBar;
import com.esri.mo2.ui.tb.SelectionToolBar;

public class CoordinateLine extends JFrame {
	static Map map = new Map();
	Layer layer = new Layer();
	Layer layer2 = new Layer();
	com.esri.mo2.cs.geom.Envelope r;
	AcetateLayer acetLayer = null;
	Toc toc = new Toc();
	String s1 = "C:\\ESRI\\MOJ20\\Samples\\Data\\USA\\States.shp";
	String s2 = "C:\\ESRI\\MOJ20\\Samples\\Data\\USA\\capitals.shp";
	ZoomPanToolBar zptb = new ZoomPanToolBar();
	SelectionToolBar stb = new SelectionToolBar();
	JPanel myjp = new JPanel();
	JButton myjb = new JButton("select[1]");
	ActionListener actlis;	
	TocAdapter mytocadapter;
	
// 	********Added by Travis Dattilo********
	
	int col1,col2,col3;
	double startLatNum, startLonNum, endLatNum, endLonNum; 
	String startLatStr, startLonStr, endLatStr, endLonStr;
	String selected1,selected2,selected3,selected4; 

	JFrame lonlatFrame = new JFrame("Draw a Line between Coordinates");	 
	JFrame readmeFrame = new JFrame("README");
	
	JPanel howtoPanel = new JPanel();
	JPanel textPanel1 = new JPanel();
	JPanel textPanel2 = new JPanel();
	JPanel textPanel3 = new JPanel();
	JPanel textPanel4 = new JPanel();
	JPanel buttonPanel = new JPanel();
	JPanel startPanel = new JPanel();
	
	JTextPane readmeInfo = new JTextPane();
	SimpleAttributeSet set = new SimpleAttributeSet();
	
	JLabel howtoLabel = new JLabel(" How to use:  ");
	JLabel startLabelLat = new JLabel(" First Point Latitude: ");
	JLabel startLabelLon = new JLabel(" First Point Longitude: ");
	JLabel endLabelLat = new JLabel(" Second Point Latitude: ");
	JLabel endLabelLon = new JLabel(" Second Point Longitude: ");	
	
	JLabel error1 = new JLabel("*Invalid Input*");
	JLabel error2 = new JLabel("*Invalid Input*");
	JLabel error3 = new JLabel("*Invalid Input*");
	JLabel error4 = new JLabel("*Invalid Input*");
	
	JLabel degree1 = new JLabel("\u00b0");
	JLabel degree2 = new JLabel("\u00b0");
	JLabel degree3 = new JLabel("\u00b0");
	JLabel degree4 = new JLabel("\u00b0");
		
	JTextField startLat = new JTextField(5);
	JTextField startLon = new JTextField(5);
	JTextField endLat = new JTextField(5);
	JTextField endLon = new JTextField(5);
	
	JButton readmeButton = new JButton("README");
	JButton DrawLine = new JButton("Draw Lines");
	JButton redLine = new JButton("Red");
	JButton greenLine = new JButton("Green");
	JButton blueLine = new JButton("Blue");
	JButton blackLine = new JButton("Black");	
	JButton startButton = new JButton("Draw Now");
	
	JComboBox dropdown1 = new JComboBox();
	JComboBox dropdown2 = new JComboBox(); 
	JComboBox dropdown3 = new JComboBox(); 
	JComboBox dropdown4 = new JComboBox(); 
	
	Font italicFont = new Font("",Font.ITALIC,12);
	
	public CoordinateLine() {
		super("Quick Start");
		this.setBounds(80,80,750, 550);
		zptb.setMap(map);
		stb.setMap(map);
		//map.addLayerListener(toc);
		actlis = new ActionListener (){
			public void actionPerformed(ActionEvent ae){
		 		stb.setSelectedLayer(map.getLayer(1));
		 		System.out.println(map.getLayer(1).getName());//this is same as the name that appears in the table of contents
			}
		};
		toc.setMap(map);
		mytocadapter = new TocAdapter() {
	     	public void click(TocEvent e) {
		    	System.out.println("aloha");
		    	stb.setSelectedLayer((e.getLegend()).getLayer());
	     	}
	    };
	    toc.addTocListener(mytocadapter);
	    myjb.addActionListener(actlis);
	    myjp.add(zptb); 
	    myjp.add(stb); 
	    myjp.add(myjb);
	    getContentPane().add(map, BorderLayout.CENTER);
	    getContentPane().add(myjp,BorderLayout.NORTH);
	    addShapefileToMap(layer,s1);
	    addShapefileToMap(layer2,s2);
	    final Point pt = new Point(-117,33);
	    getContentPane().add(toc, BorderLayout.WEST);   
	    
	    
// 		********New JFrame Window********
	    
	    lonlatFrame.setVisible(false);	
	    myjp.add(DrawLine);
	    Container frame = lonlatFrame.getContentPane();
	    lonlatFrame.setSize(390,350);
	    
	    frame.setLayout(new FlowLayout(FlowLayout.LEFT));
	    DrawLine.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		lonlatFrame.setVisible(true);	
	    	}
	    });
	    
	    readmeButton.setBackground(Color.WHITE);
	    readmeFrame.setVisible(false);
	    Container frame2 = readmeFrame.getContentPane();
	    readmeFrame.setSize(410,440);
	    frame2.setLayout(new FlowLayout(FlowLayout.LEFT));
	    readmeInfo.setCharacterAttributes(set, true);
	    readmeInfo.setEditable(false);
	    readmeInfo.setText("Travis Dattilo\n"
	    				 + "CS537 GIS\n"
	    				 + "README\n\n"
	    				 + "Enter coordinates of latitude and longitude degrees as decimal    \n"
	    				 + "numbers in the available text fields. Using the the dropdown      \n"
	    				 + "menu, specify the cardinal direction, North, East, South, or      \n"
	    				 + "West, for each degree of latitude or longitude. If the user       \n"
	    				 + "mistypes or enters an invalid entry into the text fields, they    \n"
	    				 + "will be notified on screen and no coordinate lines will be drawn. \n"
	    				 + "Click on one of the line color buttons or you can use the default \n"
	    				 + "black line color. Once the user has entered two coordinate pairs  \n"
	    				 + "and selected a line color, two points will be drawn on the map    \n"
	    				 + "and a line will be slowly drawn between them.   \n\n"
	    				 + "The user may then clear their entries and draw more lines 	  \n"
	    				 + "between points on the map (as many as they want).\n\n" 
	    				 + "SPECIAL NOTE: This map depicts the United States of America and   \n"
	    				 + "excludes surrounding countries. Additionally, for meaningful use  \n"
	    				 + "and being able to actually see the lines on the screen, the user  \n"
	    				 + "should enter points that are North latitude and West longitude.   \n"
	    				 + "However, the user has the option to draw a line to a coordinate   \n"
	    				 + "that goes beyond the bounds of the window. ");
	    frame2.add(readmeInfo);
	    
	    readmeButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		readmeFrame.setVisible(true);
	    	}
	    });
	    
	    dropdown1.addItem("North");
		dropdown1.addItem("South");
	    dropdown2.addItem("East");
		dropdown2.addItem("West");
	    dropdown3.addItem("North");
		dropdown3.addItem("South");
		dropdown4.addItem("East");
		dropdown4.addItem("West");
		
		error1.setVisible(false);
	    error1.setForeground(Color.RED);
	    error2.setVisible(false);
	    error2.setForeground(Color.RED);
	    error3.setVisible(false);
	    error3.setForeground(Color.RED);
	    error4.setVisible(false);
	    error4.setForeground(Color.RED);
	    
//		********Adding JPanels to the JFrame********
	    
	    frame.add(howtoPanel);
	    frame.add(textPanel1);	
	    frame.add(textPanel2);
	    frame.add(textPanel3);
	    frame.add(textPanel4);
	    frame.add(buttonPanel);
	    frame.add(startPanel);
	    
//		********Readme Panel********
	    
	    howtoLabel.setFont(italicFont);
	    howtoPanel.add(howtoLabel);
	    howtoPanel.add(readmeButton);
	    
//		********First Latitude Panel********
	    
	    textPanel1.add(startLabelLat);
	    textPanel1.add(startLat);
	    textPanel1.add(degree1);
	    textPanel1.add(dropdown1);	
	    textPanel1.add(error1);
	  
//		********First Longitude Panel********
	  
	    textPanel2.add(startLabelLon);
	    textPanel2.add(startLon);	
	    textPanel2.add(degree2);
	    textPanel2.add(dropdown2);	
	    textPanel2.add(error2);
	    
//		********Second Latitude Panel********	    
	    
	    textPanel3.add(endLabelLat);
	    textPanel3.add(endLat);
	    textPanel3.add(degree3);
	    textPanel3.add(dropdown3);	
	    textPanel3.add(error3);
	
//		********Second Longitude Panel********	    
	    
	    textPanel4.add(endLabelLon);
	    textPanel4.add(endLon);	   
	    textPanel4.add(degree4);
	    textPanel4.add(dropdown4);	
	    textPanel4.add(error4);
	    
//		********Color Button Panel********
	    
	    redLine.setBackground(Color.RED);			
	    greenLine.setBackground(Color.GREEN);
	    blueLine.setBackground(Color.BLUE);
	    blackLine.setBackground(Color.BLACK);
	    blueLine.setForeground(Color.WHITE);
	    blackLine.setForeground(Color.WHITE);
	    buttonPanel.add(redLine);							
	    buttonPanel.add(greenLine);
	    buttonPanel.add(blueLine);
	    buttonPanel.add(blackLine);
	    
//		********Start Button Panel********
	    
	    startButton.setPreferredSize(new Dimension(250,25));
	    startPanel.add(startButton); 
	    
// 		********Action Listeners for JTextFields********
	    
	    startLat.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		error1.setVisible(false);
	    	}
	    });
	    startLon.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		error2.setVisible(false);
	    	}
	    });
	    endLat.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		error3.setVisible(false);
	    	}
	    });
	    endLon.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		error4.setVisible(false);
	    	}
	    });
	        
// 		********Mouse Listeners for JTextFields********
	    
	    startLat.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		error1.setVisible(false);
	    	}
	    });
	    startLon.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		error2.setVisible(false);
	    	}
	    });
	    endLat.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		error3.setVisible(false);
	    	}
	    });
	    endLon.addMouseListener(new MouseAdapter() {
	    	public void mouseClicked(MouseEvent e) {
	    		error4.setVisible(false);
	    	}
	    });
	    
//		********Action Listeners for JButtons********
	    
	    redLine.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		col1 = 250;
	    		col2 = 0;
	    		col3 = 0;
	    		System.out.println("RED WAS PRESSED");
	    	}
	    });
	    
	    greenLine.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		col1 = 0;
	    		col2 = 250;
	    		col3 = 0;
	    		System.out.println("GREEN WAS PRESSED");
	    	}
	    });
	    
	    blueLine.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		col1 = 0;
	    		col2 = 0;
	    		col3 = 250;
	    		System.out.println("BLUE WAS PRESSED");
	    	}
	    });
	    
	    blackLine.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		col1 = 0;
	    		col2 = 0;
	    		col3 = 0;
	    		System.out.println("BLACK WAS PRESSED");
	    	}
	    });

//		********Action Listeners for JComboBoxes (Dropdown menu)********
	    
	    dropdown1.addActionListener( new ActionListener () {
	    	public void actionPerformed(ActionEvent e) {
	    		selected1 = dropdown1.getSelectedItem().toString(); 
	    	}
	    });
	    dropdown2.addActionListener( new ActionListener () {
	    	public void actionPerformed(ActionEvent e) {
	    		selected2 = dropdown2.getSelectedItem().toString(); 
	    	}
	    });
	    dropdown3.addActionListener( new ActionListener () {
	    	public void actionPerformed(ActionEvent e) {
	    		selected3 = dropdown3.getSelectedItem().toString(); 
	    	}
	    });
	    dropdown4.addActionListener( new ActionListener () {
	    	public void actionPerformed(ActionEvent e) {
	    		selected4 = dropdown4.getSelectedItem().toString(); 
	    	}
	    }); 
	    
//		********Start Button that Draws Lines********
	    
	    startButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) { 	
	    		int keepGoing = 0;
	    		startLatStr = startLat.getText();
	    		startLonStr = startLon.getText();
	    		endLatStr = endLat.getText();
	    		endLonStr = endLon.getText();    		
	    		
	    		error1.setVisible(false);
	    		error2.setVisible(false);
	    		error3.setVisible(false);
	    		error4.setVisible(false);
	    		
	    		try {
		    		startLatNum = Double.parseDouble(startLatStr);
	    		}catch(Exception a1) {
	    			error1.setVisible(true);
	    			keepGoing = 1;
	    		}
	    		try {
	    			startLonNum = Double.parseDouble(startLonStr);
	    		}catch(Exception a2) {
	    			error2.setVisible(true);
	    			keepGoing = 1;
	    		}
	    		try {
	    			endLatNum = Double.parseDouble(endLatStr);
	    		}catch(Exception a3) {
	    			error3.setVisible(true);
	    			keepGoing = 1;
	    		}
	    		try {
	    			endLonNum = Double.parseDouble(endLonStr);
	    		}catch(Exception a4) {
	    			error4.setVisible(true);
	    			keepGoing = 1;
	    		}
		    	
	    		if(selected1 == "South") 
	    			startLatNum *= -1;
	    		if(selected2 == "West") 
	    			startLonNum *= -1;
	    		if(selected3 == "South") 
	    			endLatNum *= -1;
	    		if(selected4 == "West") 
	    			endLonNum *= -1;
	    			    	
	    		if(keepGoing != 1) {
	    			Point2D startPair = map.transformWorldToPixel(startLonNum, startLatNum);
		    		Point2D endPair = map.transformWorldToPixel(endLonNum,endLatNum);
		    		
		    		Flash flash = new Flash(startPair.getX(),startPair.getY(),endPair.getX(),endPair.getY(), col1, col2, col3);
		    		flash.start();
	    		}
	    	}
	    });	    
	}
	
	private void addShapefileToMap(Layer layer,String s) {
		//stb.setSelectedLayer(map.getLayer(0));
		String datapath = s; //"C:\\ESRI\\MOJ20\\Samples\\Data\\USA\\States.shp";
	    layer.setDataset("0;"+datapath);
	    map.add(layer);
	}

	public static void main(String[] args) {
		CoordinateLine qstart = new CoordinateLine();
	    qstart.addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent e) {
	        System.out.println("Thanks, Quick Start exits");
	        System.exit(0);
	    }});
	    qstart.setVisible(true);
	}
}

class Flash extends Thread {
	AcetateLayer acetLayer = new AcetateLayer();
	int col1, col2, col3;
	double x1,y1,x2,y2;	
	
	Flash(double x11,double y11,double x22, double y22, int c1, int c2, int c3) {
		x1 = x11;
		y1 = y11;
		x2 = x22;
		y2 = y22;
		col1 = c1;
		col2 = c2;
		col3 = c3;	
	}	
	
	public void run() {
		for (int i=0;i<21;i++) {
			try {
				Thread.sleep(300);
				final int j = i;
				if (acetLayer != null) CoordinateLine.map.remove(acetLayer);
				acetLayer = new AcetateLayer() {
		            public void paintComponent(java.awt.Graphics g) {
						java.awt.Graphics2D g2d = (java.awt.Graphics2D) g;
						Line2D.Double line = new Line2D.Double(x1,y1,x1+j*(x2-x1)/20.0,y1+j*(y2-y1)/20.0);
						g2d.setColor(new Color(col1,col2,col3));	// Line Color 
						g2d.draw(line);
						g2d.setColor(new Color(250,0,0));			// Dot Color				
						g2d.fillOval((int)(x1-1),(int)(y1-1),5,5);
						g2d.fillOval((int)x2,(int)y2,5,5);
		            }
	            };
	            acetLayer.setMap(CoordinateLine.map);
	            CoordinateLine.map.add(acetLayer);
	      } catch (Exception e) {}
	    }
	}
}
